package com.cleanprogramming.instantconverter.data;

import android.content.Context;

import com.cleanprogramming.instantconverter.util.UpdateListener;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import io.realm.Case;
import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by Rainvic on 14/2/2017.
 */

public class RealMRepository {
    private static RealMRepository INSTANCE;
    private Realm realm;
    private Context context;

    public static RealMRepository getINSTANCE(Context context) {
        if(INSTANCE == null){
            Realm.init(context);
            INSTANCE = new RealMRepository(context);
        }
        return INSTANCE;
    }

    private RealMRepository(Context context){
        this.realm = Realm.getDefaultInstance();
        this.context = context;
        initModelCountryTable();
    }


    /**************************************************************************
     // public
     **************************************************************************/
    public void selectCurrency(final ModelCountry modelCountry, final UpdateListener transactionListener){
        if(modelCountry == null){
            transactionListener.onFailed();
            return;
        }

        ModelCurrency found = realm.where(ModelCurrency.class).equalTo("target", modelCountry.getCode(), Case.INSENSITIVE).findFirst();
        ModelCurrency modelCurrency = new ModelCurrency();
        modelCurrency.setRate(found.getRate());
        modelCurrency.setSelected(true);
        modelCurrency.setTarget(found.getTarget());
        modelCurrency.setSource(found.getSource());
        realm.beginTransaction();
        realm.copyToRealmOrUpdate(modelCurrency);
        realm.commitTransaction();
        transactionListener.onSuccess();

    }
    public void updateCurrency(final String target, final Double rate, final UpdateListener transactionListener){
        if(target == null || rate == null){
            transactionListener.onFailed();
            return;
        }

        ModelCurrency found = realm.where(ModelCurrency.class).equalTo("target", target, Case.INSENSITIVE).findFirst();
        ModelCurrency newModelCurrency = new ModelCurrency();
        newModelCurrency.setRate(rate);
        newModelCurrency.setSelected(found.isSelected());
        newModelCurrency.setTarget(found.getTarget());
        newModelCurrency.setSource(found.getSource());
        realm.beginTransaction();
        realm.copyToRealmOrUpdate(newModelCurrency);
        realm.commitTransaction();
        transactionListener.onSuccess();

    }

    public void deleteCurrency(final ModelCountry modelCountry, final UpdateListener transactionListener){
        if(modelCountry == null){
            transactionListener.onFailed();
            return;
        }

        ModelCurrency found = realm.where(ModelCurrency.class).equalTo("target", modelCountry.getCode(), Case.INSENSITIVE).findFirst();
        ModelCurrency modelCurrency = new ModelCurrency();
        modelCurrency.setRate(found.getRate());
        modelCurrency.setSelected(false);
        modelCurrency.setTarget(found.getTarget());
        modelCurrency.setSource(found.getSource());
        realm.beginTransaction();
        realm.copyToRealmOrUpdate(modelCurrency);
        realm.commitTransaction();
        transactionListener.onSuccess();
    }

    public List<ModelCurrency> getSelectedCurrencies(){
        RealmResults<ModelCurrency> modelCurrencies = realm.where(ModelCurrency.class).equalTo("selected", true).findAll();
        List<ModelCurrency> modelCurrencyList = realm.copyFromRealm(modelCurrencies);
        return modelCurrencyList;
    }

    public void initModelCountryTable(){

        if(realm.where(ModelCountry.class).findFirst() == null) {

            try {
                final JSONArray jsonArrayString = new JSONArray(loadModelCountryJSONFromAsset());

                // Insert multiple items using an InputStream
                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        realm.createAllFromJson(ModelCountry.class, jsonArrayString);
                    }
                });
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

    }

    public List<ModelCurrency> getAllModelCurrency(){
        RealmResults<ModelCurrency> modelCurrencies = realm.where(ModelCurrency.class).findAll();
        List<ModelCurrency> modelCurrencyList = realm.copyFromRealm(modelCurrencies);
        return modelCurrencyList;
    }

    public List<ModelCountry> queryStringForAllColumns(String query){
//        RealmResults<ModelCountry> modelCountries = realm.where(ModelCountry.class).findAll();
//        List<ModelCountry> modelCountryList = realm.copyFromRealm(modelCountries);

//
        RealmResults<ModelCountry> modelCountries = realm.where(ModelCountry.class).contains("country", query, Case.INSENSITIVE).findAll();
        List<ModelCountry> modelCountryList = realm.copyFromRealm(modelCountries);

        modelCountries = realm.where(ModelCountry.class).contains("country_code", query, Case.INSENSITIVE).findAll();
        modelCountryList.addAll(realm.copyFromRealm(modelCountries));

        modelCountries = realm.where(ModelCountry.class).contains("currency", query, Case.INSENSITIVE).findAll();
        modelCountryList.addAll(realm.copyFromRealm(modelCountries));

        modelCountries = realm.where(ModelCountry.class).contains("code", query, Case.INSENSITIVE).findAll();
        modelCountryList.addAll(realm.copyFromRealm(modelCountries));


        Set<ModelCountry> set = new HashSet<>();
        set.addAll(modelCountryList);

        modelCountryList.clear();
        modelCountryList.addAll(set);

        return modelCountryList;
    }

    /**************************************************************************
     // private
     **************************************************************************/

    public String loadModelCountryJSONFromAsset() {
        String json = null;
        try {
            InputStream is = context.getAssets().open("country_currency.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

}
