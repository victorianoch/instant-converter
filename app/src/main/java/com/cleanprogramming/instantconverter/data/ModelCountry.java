package com.cleanprogramming.instantconverter.data;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Rainvic on 14/2/2017.
 */

public class ModelCountry extends RealmObject {

    @PrimaryKey
    String country;
    String country_code;
    String currency;
    String code;

    public ModelCountry(String country, String country_code, String currency, String code) {
        this.country = country;
        this.country_code = country_code;
        this.currency = currency;
        this.code = code;
    }

    public ModelCountry() {}

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCountry_code() {
        return country_code;
    }

    public void setCountry_code(String country_code) {
        this.country_code = country_code;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
