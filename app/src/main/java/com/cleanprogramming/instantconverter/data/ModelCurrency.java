package com.cleanprogramming.instantconverter.data;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Rainvic on 13/2/2017.
 */

public class ModelCurrency extends RealmObject{

    String source;
    @PrimaryKey
    String target;
    Double rate;
    boolean selected;

    public ModelCurrency(String source, String target, Double rate, boolean selected) {
        this.source = source;
        this.target = target;
        this.rate = rate;
        this.selected = selected;
    }

    public ModelCurrency() {
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public Double getRate() {
        return rate;
    }

    public void setRate(Double rate) {
        this.rate = rate;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }
}
