package com.cleanprogramming.instantconverter.data;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;

import com.cleanprogramming.instantconverter.util.PreferenceUtil;
import com.cleanprogramming.instantconverter.util.UpdateListener;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import io.realm.Realm;

/**
 * Created by Rainvic on 13/2/2017.
 */

public class FBRepository {

    private static final String TAG = FBRepository.class.getSimpleName();

    static private FBRepository _instance;

    private Context context;

    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;

    private boolean signIn = false;

    // Write a message to the database
    FirebaseDatabase database;
    DatabaseReference instantCurrencyRef;

    static {
        FirebaseDatabase.getInstance().setPersistenceEnabled(true);
    }

    public static FBRepository getInstance(Context context){
        if(_instance == null){
            _instance = new FBRepository(context);
        }
        return _instance;
    }


    private FBRepository(Context context){
        this.context = context;
        // Write a message to the database
        database = FirebaseDatabase.getInstance();
        instantCurrencyRef = database.getReference("instant-currency");
        mAuth = FirebaseAuth.getInstance();
        mAuthListener = getFirebaseAuthListener();
        Realm.init(context);
    }

    public void checkUpdateAvailableNUpdate(final UpdateListener updateListener){
        DatabaseReference timeStampRef = database.getReference("timestamp");

        // Read from the database
        timeStampRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                Long prevTimeStamp = PreferenceUtil.getUpdateTimeStamp(context);
                Long curTimeStamp = dataSnapshot.getValue(Long.class);

                if(prevTimeStamp == -1) {
                    getCurrencies(updateListener);
                }else if(prevTimeStamp < curTimeStamp){
                    updateCurrencies(updateListener);
                }else {
                    updateListener.onSuccess();
                }

                PreferenceUtil.setUpdateTimeStamp(context, curTimeStamp);
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.w(TAG, "Failed to read value.", error.toException());
                updateListener.onFailed();
            }
        });
    }

    public void updateCurrencies(final UpdateListener updateListener){

        // Write a message to the database
        DatabaseReference quotesRef = database.getReference("quotes");

        // Read from the database
        quotesRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                Realm realm = Realm.getDefaultInstance();

                if(realm.isEmpty()){

                    realm.beginTransaction();
                    for (DataSnapshot postSnapshot: dataSnapshot.getChildren()) {

                        Double currency = postSnapshot.getValue(Double.class);
                        String srcNTgt = postSnapshot.getKey();
                        ModelCurrency modelCurrency = realm.createObject(ModelCurrency.class, srcNTgt.substring(3));

                        modelCurrency.setSource(srcNTgt.substring(0, 3));
                        modelCurrency.setRate(currency);
                        if(srcNTgt.substring(3).equals("USD")){
                            modelCurrency.setSelected(true);
                        }else {
                            modelCurrency.setSelected(false);
                        }
                    }

                    realm.commitTransaction();
                }else{


                    for (DataSnapshot postSnapshot: dataSnapshot.getChildren()) {

                        Double currency = postSnapshot.getValue(Double.class);
                        String srcNTgt = postSnapshot.getKey();

                        RealMRepository realMRepository = RealMRepository.getINSTANCE(context);
                        realMRepository.updateCurrency(srcNTgt.substring(3), currency, new UpdateListener() {
                            @Override
                            public void onSuccess() {

                            }

                            @Override
                            public void onFailed() {
                                updateListener.onFailed();
                            }
                        });
                    }

                }

                updateListener.onSuccess();
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.w(TAG, "Failed to read value.", error.toException());
                updateListener.onFailed();
            }
        });
    }

    public void getCurrencies(final UpdateListener updateListener){

        // Write a message to the database
        DatabaseReference quotesRef = database.getReference("quotes");

        // Read from the database
        quotesRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                Realm realm = Realm.getDefaultInstance();
                realm.beginTransaction();

                if(!realm.isEmpty()){
                    realm.delete(ModelCurrency.class);
                }
                for (DataSnapshot postSnapshot: dataSnapshot.getChildren()) {

                    Double currency = postSnapshot.getValue(Double.class);
                    String srcNTgt = postSnapshot.getKey();
                    ModelCurrency modelCurrency = realm.createObject(ModelCurrency.class, srcNTgt.substring(3));

                    modelCurrency.setSource(srcNTgt.substring(0, 3));
                    modelCurrency.setRate(currency);
                    if(srcNTgt.substring(3).equals("USD")){
                        modelCurrency.setSelected(true);
                    }else {
                        modelCurrency.setSelected(false);
                    }
                }

//                //add usd
//                ModelCurrency usdCurrency = realm.createObject(ModelCurrency.class, "USD");
//
//                usdCurrency.setSource("USD");
//                usdCurrency.setRate(1d);
//                usdCurrency.setSelected(true);
                realm.commitTransaction();

                updateListener.onSuccess();
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.w(TAG, "Failed to read value.", error.toException());
                updateListener.onFailed();
            }
        });
    }

    public void signInUser(final UpdateListener updateListener){
        mAuth.addAuthStateListener(mAuthListener);
        mAuth.signInAnonymously()
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        Log.d(TAG, "signInAnonymously:onComplete:" + task.isSuccessful());

                        // If sign in fails, display a message to the user. If sign in succeeds
                        // the auth state listener will be notified and logic to handle the
                        // signed in user can be handled in the listener.
                        if (!task.isSuccessful()) {
                            Log.w(TAG, "signInAnonymously", task.getException());
                            updateListener.onFailed();
                        }else{
                            signIn = true;
                            updateListener.onSuccess();
                        }
                    }
                });
    }

    /**************************************************************************
     // private
     **************************************************************************/
    private FirebaseAuth.AuthStateListener getFirebaseAuthListener(){
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    // User is signed in
                    Log.d(TAG, "onAuthStateChanged:signed_in:" + user.getUid());
                } else {
                    // User is signed out
                    Log.d(TAG, "onAuthStateChanged:signed_out");
                }
                // ...
            }
        };
        return mAuthListener;
    }
}
