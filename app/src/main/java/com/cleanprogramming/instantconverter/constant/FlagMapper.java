package com.cleanprogramming.instantconverter.constant;

import com.cleanprogramming.instantconverter.R;

import java.util.HashMap;
import java.util.Map;


/**
 * Created by victoryang on 24/8/16.
 */
public final class FlagMapper {

    private static FlagMapper _instance = new FlagMapper();
    private final Map<String, Integer> flags;

    public final static void init(){
        Map<String, Integer> tempFlags = new HashMap<>();
//        tempFlags.put("AFG",R.drawable.af);
//        tempFlags.put("ALB",R.drawable.al);
//        tempFlags.put("DZA",R.drawable.dz);
//        tempFlags.put("ASM",R.drawable.as);
//        tempFlags.put("AND",R.drawable.ad);
//        tempFlags.put("AGO",R.drawable.ao);
//        tempFlags.put("AIA",R.drawable.ai);
//        tempFlags.put("ATG",R.drawable.ag);
//        tempFlags.put("ARG",R.drawable.ar);
//        tempFlags.put("ARM",R.drawable.am);
//        tempFlags.put("ABW",R.drawable.aw);
//        tempFlags.put("AUS",R.drawable.au);
//        tempFlags.put("AUT",R.drawable.at);
//        tempFlags.put("AZE",R.drawable.az);
//        tempFlags.put("BHS",R.drawable.bs);
//        tempFlags.put("BHR",R.drawable.bh);
//        tempFlags.put("BGD",R.drawable.bd);
//        tempFlags.put("BRB",R.drawable.bb);
//        tempFlags.put("BLR",R.drawable.by);
//        tempFlags.put("BEL",R.drawable.be);
//        tempFlags.put("BLZ",R.drawable.bz);
//        tempFlags.put("BEN",R.drawable.bj);
//        tempFlags.put("BMU",R.drawable.bm);
//        tempFlags.put("BTN",R.drawable.bt);
//        tempFlags.put("BOL",R.drawable.bo);
//        tempFlags.put("BIH",R.drawable.ba);
//        tempFlags.put("BWA",R.drawable.bw);
//        tempFlags.put("BRA",R.drawable.br);
//        tempFlags.put("BRN",R.drawable.bn);
//        tempFlags.put("BGR",R.drawable.bg);
//        tempFlags.put("BFA",R.drawable.bf);
//        tempFlags.put("BDI",R.drawable.bi);
//        tempFlags.put("CPV",R.drawable.cv);
//        tempFlags.put("KHM",R.drawable.kh);
//        tempFlags.put("CMR",R.drawable.cm);
//        tempFlags.put("CAN",R.drawable.ca);
//        tempFlags.put("CYM",R.drawable.ky);
//        tempFlags.put("CAF",R.drawable.cf);
//        tempFlags.put("TCD",R.drawable.td);
//        tempFlags.put("CHL",R.drawable.cl);
//        tempFlags.put("CHN",R.drawable.cn);
//        tempFlags.put("COL",R.drawable.co);
//        tempFlags.put("COM",R.drawable.km);
//        tempFlags.put("COG",R.drawable.cg);
//        tempFlags.put("COD",R.drawable.cd);
//        tempFlags.put("COK",R.drawable.ck);
//        tempFlags.put("CRI",R.drawable.cr);
//        tempFlags.put("CIV",R.drawable.ci);
//        tempFlags.put("HRV",R.drawable.hr);
//        tempFlags.put("CUB",R.drawable.cu);
//        tempFlags.put("CYP",R.drawable.cy);
//        tempFlags.put("CZE",R.drawable.cz);
//        tempFlags.put("DNK",R.drawable.dk);
//        tempFlags.put("DJI",R.drawable.dj);
//        tempFlags.put("DMA",R.drawable.dm);
//        tempFlags.put("ECU",R.drawable.ec);
//        tempFlags.put("EGY",R.drawable.eg);
//        tempFlags.put("SLV",R.drawable.sv);
//        tempFlags.put("GNQ",R.drawable.gq);
//        tempFlags.put("ERI",R.drawable.er);
//        tempFlags.put("EST",R.drawable.ee);
//        tempFlags.put("ETH",R.drawable.et);
//        tempFlags.put("FRO",R.drawable.fo);
//        tempFlags.put("FJI",R.drawable.fj);
//        tempFlags.put("FIN",R.drawable.fi);
//        tempFlags.put("FRA",R.drawable.fr);
//        tempFlags.put("PYF",R.drawable.pf);
//        tempFlags.put("GAB",R.drawable.ga);
//        tempFlags.put("GMB",R.drawable.gm);
//        tempFlags.put("GEO",R.drawable.ge);
//        tempFlags.put("DEU",R.drawable.de);
//        tempFlags.put("GHA",R.drawable.gh);
//        tempFlags.put("GIB",R.drawable.gi);
//        tempFlags.put("GRC",R.drawable.gr);
//        tempFlags.put("GRL",R.drawable.gl);
//        tempFlags.put("GRD",R.drawable.gd);
//        tempFlags.put("GLP",R.drawable.gp);
//        tempFlags.put("GUM",R.drawable.gu);
//        tempFlags.put("GTM",R.drawable.gt);
//        tempFlags.put("GIN",R.drawable.gn);
//        tempFlags.put("GNB",R.drawable.gw);
//        tempFlags.put("GUY",R.drawable.gy);
//        tempFlags.put("HTI",R.drawable.ht);
//        tempFlags.put("VAT",R.drawable.va);
//        tempFlags.put("HND",R.drawable.hn);
//        tempFlags.put("HKG",R.drawable.hk);
//        tempFlags.put("HUN",R.drawable.hu);
//        tempFlags.put("ISL",R.drawable.is);
//        tempFlags.put("IND",R.drawable.in);
//        tempFlags.put("IDN",R.drawable.id);
//        tempFlags.put("IRN",R.drawable.ir);
//        tempFlags.put("IRQ",R.drawable.iq);
//        tempFlags.put("IRL",R.drawable.ie);
//        tempFlags.put("ISR",R.drawable.il);
//        tempFlags.put("ITA",R.drawable.it);
//        tempFlags.put("JAM",R.drawable.jm);
//        tempFlags.put("JPN",R.drawable.jp);
//        tempFlags.put("JOR",R.drawable.jo);
//        tempFlags.put("KAZ",R.drawable.kz);
//        tempFlags.put("KEN",R.drawable.ke);
//        tempFlags.put("KIR",R.drawable.ki);
//        tempFlags.put("PRK",R.drawable.kp);
//        tempFlags.put("KOR",R.drawable.kr);
//        tempFlags.put("KWT",R.drawable.kw);
//        tempFlags.put("KGZ",R.drawable.kg);
//        tempFlags.put("LAO",R.drawable.la);
//        tempFlags.put("LVA",R.drawable.lv);
//        tempFlags.put("LBN",R.drawable.lb);
//        tempFlags.put("LSO",R.drawable.ls);
//        tempFlags.put("LBR",R.drawable.lr);
//        tempFlags.put("LBY",R.drawable.ly);
//        tempFlags.put("LIE",R.drawable.li);
//        tempFlags.put("LTU",R.drawable.lt);
//        tempFlags.put("LUX",R.drawable.lu);
//        tempFlags.put("MAC",R.drawable.mo);
//        tempFlags.put("MKD",R.drawable.mk);
//        tempFlags.put("MDG",R.drawable.mg);
//        tempFlags.put("MWI",R.drawable.mw);
//        tempFlags.put("MYS",R.drawable.my);
//        tempFlags.put("MDV",R.drawable.mv);
//        tempFlags.put("MLI",R.drawable.ml);
//        tempFlags.put("MLT",R.drawable.mt);
//        tempFlags.put("MHL",R.drawable.mh);
//        tempFlags.put("MTQ",R.drawable.mq);
//        tempFlags.put("MRT",R.drawable.mr);
//        tempFlags.put("MUS",R.drawable.mu);
//        tempFlags.put("MEX",R.drawable.mx);
//        tempFlags.put("FSM",R.drawable.fm);
//        tempFlags.put("MDA",R.drawable.md);
//        tempFlags.put("MCO",R.drawable.mc);
//        tempFlags.put("MNG",R.drawable.mn);
//        tempFlags.put("MNE",R.drawable.me);
//        tempFlags.put("MSR",R.drawable.ms);
//        tempFlags.put("MAR",R.drawable.ma);
//        tempFlags.put("MOZ",R.drawable.mz);
//        tempFlags.put("MMR",R.drawable.mm);
//        tempFlags.put("NAM",R.drawable.na);
//        tempFlags.put("NRU",R.drawable.nr);
//        tempFlags.put("NPL",R.drawable.np);
//        tempFlags.put("NLD",R.drawable.nl);
//        tempFlags.put("NCL",R.drawable.nc);
//        tempFlags.put("NZL",R.drawable.nz);
//        tempFlags.put("NIC",R.drawable.ni);
//        tempFlags.put("NER",R.drawable.ne);
//        tempFlags.put("NGA",R.drawable.ng);
//        tempFlags.put("NOR",R.drawable.no);
//        tempFlags.put("OMN",R.drawable.om);
//        tempFlags.put("PAK",R.drawable.pk);
//        tempFlags.put("PLW",R.drawable.pw);
//        tempFlags.put("PSE",R.drawable.ps);
//        tempFlags.put("PAN",R.drawable.pa);
//        tempFlags.put("PNG",R.drawable.pg);
//        tempFlags.put("PRY",R.drawable.py);
//        tempFlags.put("PER",R.drawable.pe);
//        tempFlags.put("PHL",R.drawable.ph);
//        tempFlags.put("POL",R.drawable.pl);
//        tempFlags.put("PRT",R.drawable.pt);
//        tempFlags.put("PRI",R.drawable.pr);
//        tempFlags.put("QAT",R.drawable.qa);
//        tempFlags.put("REU",R.drawable.re);
//        tempFlags.put("ROU",R.drawable.ro);
//        tempFlags.put("RUS",R.drawable.ru);
//        tempFlags.put("RWA",R.drawable.rw);
//        tempFlags.put("KNA",R.drawable.kn);
//        tempFlags.put("LCA",R.drawable.lc);
//        tempFlags.put("VCT",R.drawable.vc);
//        tempFlags.put("WSM",R.drawable.ws);
//        tempFlags.put("SMR",R.drawable.sm);
//        tempFlags.put("STP",R.drawable.st);
//        tempFlags.put("SAU",R.drawable.sa);
//        tempFlags.put("SEN",R.drawable.sn);
//        tempFlags.put("SRB",R.drawable.rs);
//        tempFlags.put("SYC",R.drawable.sc);
//        tempFlags.put("SLE",R.drawable.sl);
//        tempFlags.put("SGP",R.drawable.sg);
//        tempFlags.put("SVK",R.drawable.sk);
//        tempFlags.put("SVN",R.drawable.si);
//        tempFlags.put("SLB",R.drawable.sb);
//        tempFlags.put("SOM",R.drawable.so);
//        tempFlags.put("ZAF",R.drawable.za);
//        tempFlags.put("ESP",R.drawable.es);
//        tempFlags.put("LKA",R.drawable.lk);
//        tempFlags.put("SDN",R.drawable.sd);
//        tempFlags.put("SUR",R.drawable.sr);
//        tempFlags.put("SWZ",R.drawable.sz);
//        tempFlags.put("SWE",R.drawable.se);
//        tempFlags.put("CHE",R.drawable.ch);
//        tempFlags.put("SYR",R.drawable.sy);
//        tempFlags.put("TWN",R.drawable.tw);
//        tempFlags.put("TJK",R.drawable.tj);
//        tempFlags.put("TZA",R.drawable.tz);
//        tempFlags.put("THA",R.drawable.th);
//        tempFlags.put("TLS",R.drawable.tl);
//        tempFlags.put("TGO",R.drawable.tg);
//        tempFlags.put("TON",R.drawable.to);
//        tempFlags.put("TTO",R.drawable.tt);
//        tempFlags.put("TUN",R.drawable.tn);
//        tempFlags.put("TUR",R.drawable.tr);
//        tempFlags.put("TKM",R.drawable.tm);
//        tempFlags.put("TCA",R.drawable.tc);
//        tempFlags.put("TUV",R.drawable.tv);
//        tempFlags.put("UGA",R.drawable.ug);
//        tempFlags.put("UKR",R.drawable.ua);
//        tempFlags.put("ARE",R.drawable.ae);
//        tempFlags.put("GBR",R.drawable.gb);
//        tempFlags.put("USA",R.drawable.us);
//        tempFlags.put("URY",R.drawable.uy);
//        tempFlags.put("UZB",R.drawable.uz);
//        tempFlags.put("VUT",R.drawable.vu);
//        tempFlags.put("VEN",R.drawable.ve);
//        tempFlags.put("VNM",R.drawable.vn);
//        tempFlags.put("VGB",R.drawable.vg);
//        tempFlags.put("VIR",R.drawable.vi);
//        tempFlags.put("ESH",R.drawable.eh);
//        tempFlags.put("YEM",R.drawable.ye);
//        tempFlags.put("ZMB",R.drawable.zm);
//        tempFlags.put("ZWE",R.drawable.zw);
















tempFlags.put("NZD", R.drawable.nz);
tempFlags.put("NZD", R.drawable.ck);
tempFlags.put("AUD", R.drawable.au);
tempFlags.put("AUD", R.drawable.ki);
tempFlags.put("AUD", R.drawable.nr);
tempFlags.put("AUD", R.drawable.tv);
tempFlags.put("EUR", R.drawable.as);
tempFlags.put("EUR", R.drawable.ad);
tempFlags.put("EUR", R.drawable.at);
tempFlags.put("EUR", R.drawable.be);
tempFlags.put("EUR", R.drawable.fi);
tempFlags.put("EUR", R.drawable.fr);
tempFlags.put("EUR", R.drawable.de);
tempFlags.put("EUR", R.drawable.gr);
tempFlags.put("EUR", R.drawable.gp);
tempFlags.put("EUR", R.drawable.ie);
tempFlags.put("EUR", R.drawable.it);
tempFlags.put("EUR", R.drawable.lu);
tempFlags.put("EUR", R.drawable.mq);
tempFlags.put("EUR", R.drawable.mc);
tempFlags.put("EUR", R.drawable.nl);
tempFlags.put("EUR", R.drawable.pt);
tempFlags.put("EUR", R.drawable.re);
tempFlags.put("EUR", R.drawable.ws);
tempFlags.put("EUR", R.drawable.sm);
tempFlags.put("EUR", R.drawable.si);
tempFlags.put("EUR", R.drawable.es);
tempFlags.put("EUR", R.drawable.va);
tempFlags.put("GBP", R.drawable.gb);
tempFlags.put("GBP", R.drawable.je);
tempFlags.put("USD", R.drawable.gu);
tempFlags.put("USD", R.drawable.mh);
tempFlags.put("USD", R.drawable.fm);
tempFlags.put("USD", R.drawable.pw);
tempFlags.put("USD", R.drawable.pr);
tempFlags.put("USD", R.drawable.tc);
tempFlags.put("USD", R.drawable.us);
tempFlags.put("USD", R.drawable.vg);
tempFlags.put("USD", R.drawable.vi);
tempFlags.put("HKD", R.drawable.hk);
tempFlags.put("CAD", R.drawable.ca);
tempFlags.put("JPY", R.drawable.jp);
tempFlags.put("AFN", R.drawable.af);
tempFlags.put("ALL", R.drawable.al);
tempFlags.put("DZD", R.drawable.dz);
tempFlags.put("XCD", R.drawable.ai);
tempFlags.put("XCD", R.drawable.ag);
tempFlags.put("XCD", R.drawable.dm);
tempFlags.put("XCD", R.drawable.gd);
tempFlags.put("XCD", R.drawable.ms);
tempFlags.put("XCD", R.drawable.kn);
tempFlags.put("XCD", R.drawable.lc);
tempFlags.put("XCD", R.drawable.vc);
tempFlags.put("ARS", R.drawable.ar);
tempFlags.put("AMD", R.drawable.am);
tempFlags.put("ANG", R.drawable.aw);
tempFlags.put("ANG", R.drawable.an);
tempFlags.put("AZN", R.drawable.az);
tempFlags.put("BSD", R.drawable.bs);
tempFlags.put("BHD", R.drawable.bh);
tempFlags.put("BDT", R.drawable.bd);
tempFlags.put("BBD", R.drawable.bb);
tempFlags.put("BYR", R.drawable.by);
tempFlags.put("BZD", R.drawable.bz);
tempFlags.put("XOF", R.drawable.bj);
tempFlags.put("XOF", R.drawable.bf);
tempFlags.put("XOF", R.drawable.gw);
tempFlags.put("XOF", R.drawable.ci);
tempFlags.put("XOF", R.drawable.ml);
tempFlags.put("XOF", R.drawable.ne);
tempFlags.put("XOF", R.drawable.sn);
tempFlags.put("XOF", R.drawable.tg);
tempFlags.put("BMD", R.drawable.bm);
tempFlags.put("INR", R.drawable.bt);
tempFlags.put("INR", R.drawable.in);
tempFlags.put("BOB", R.drawable.bo);
tempFlags.put("BWP", R.drawable.bw);
tempFlags.put("NOK", R.drawable.no);
tempFlags.put("BRL", R.drawable.br);
tempFlags.put("BND", R.drawable.bn);
tempFlags.put("BGN", R.drawable.bg);
tempFlags.put("BIF", R.drawable.bi);
tempFlags.put("KHR", R.drawable.kh);
tempFlags.put("XAF", R.drawable.cm);
tempFlags.put("XAF", R.drawable.cf);
tempFlags.put("XAF", R.drawable.td);
tempFlags.put("XAF", R.drawable.cg);
tempFlags.put("XAF", R.drawable.gq);
tempFlags.put("XAF", R.drawable.ga);
tempFlags.put("CVE", R.drawable.cv);
tempFlags.put("KYD", R.drawable.ky);
tempFlags.put("CLP", R.drawable.cl);
tempFlags.put("CNY", R.drawable.cn);
tempFlags.put("COP", R.drawable.co);
tempFlags.put("KMF", R.drawable.km);
tempFlags.put("CDF", R.drawable.cd);
tempFlags.put("CRC", R.drawable.cr);
tempFlags.put("HRK", R.drawable.hr);
tempFlags.put("CUP", R.drawable.cu);
tempFlags.put("CYP", R.drawable.cy);
tempFlags.put("CZK", R.drawable.cz);
tempFlags.put("DKK", R.drawable.dk);
tempFlags.put("DKK", R.drawable.fo);
tempFlags.put("DKK", R.drawable.gl);
tempFlags.put("DJF", R.drawable.dj);
tempFlags.put("IDR", R.drawable.id);
tempFlags.put("ECS", R.drawable.ec);
tempFlags.put("EGP", R.drawable.eg);
tempFlags.put("SVC", R.drawable.sv);
tempFlags.put("ETB", R.drawable.er);
tempFlags.put("ETB", R.drawable.et);
tempFlags.put("EEK", R.drawable.ee);
tempFlags.put("FJD", R.drawable.fj);
tempFlags.put("XPF", R.drawable.pf);
tempFlags.put("XPF", R.drawable.nc);
tempFlags.put("GMD", R.drawable.gm);
tempFlags.put("GEL", R.drawable.ge);
tempFlags.put("GIP", R.drawable.gi);
tempFlags.put("GTQ", R.drawable.gt);
tempFlags.put("GNF", R.drawable.gn);
tempFlags.put("GYD", R.drawable.gy);
tempFlags.put("HTG", R.drawable.ht);
tempFlags.put("HNL", R.drawable.hn);
tempFlags.put("HUF", R.drawable.hu);
tempFlags.put("ISK", R.drawable.is);
tempFlags.put("IRR", R.drawable.ir);
tempFlags.put("IQD", R.drawable.iq);
tempFlags.put("ILS", R.drawable.il);
tempFlags.put("JMD", R.drawable.jm);
tempFlags.put("JOD", R.drawable.jo);
tempFlags.put("KZT", R.drawable.kz);
tempFlags.put("KES", R.drawable.ke);
tempFlags.put("KPW", R.drawable.kp);
tempFlags.put("KRW", R.drawable.kr);
tempFlags.put("KWD", R.drawable.kw);
tempFlags.put("KGS", R.drawable.kg);
tempFlags.put("LAK", R.drawable.la);
tempFlags.put("LVL", R.drawable.lv);
tempFlags.put("LBP", R.drawable.lb);
tempFlags.put("LSL", R.drawable.ls);
tempFlags.put("LRD", R.drawable.lr);
tempFlags.put("LYD", R.drawable.ly);
tempFlags.put("CHF", R.drawable.li);
tempFlags.put("CHF", R.drawable.ch);
tempFlags.put("LTL", R.drawable.lt);
tempFlags.put("MOP", R.drawable.mo);
tempFlags.put("MKD", R.drawable.mk);
tempFlags.put("MGA", R.drawable.mg);
tempFlags.put("MWK", R.drawable.mw);
tempFlags.put("MYR", R.drawable.my);
tempFlags.put("MVR", R.drawable.mv);
tempFlags.put("MTL", R.drawable.mt);
tempFlags.put("MRO", R.drawable.mr);
tempFlags.put("MUR", R.drawable.mu);
tempFlags.put("MXN", R.drawable.mx);
tempFlags.put("MDL", R.drawable.md);
tempFlags.put("MNT", R.drawable.mn);
tempFlags.put("MAD", R.drawable.ma);
tempFlags.put("MAD", R.drawable.eh);
tempFlags.put("MZN", R.drawable.mz);
tempFlags.put("MMK", R.drawable.mm);
tempFlags.put("NAD", R.drawable.na);
tempFlags.put("NPR", R.drawable.np);
tempFlags.put("NIO", R.drawable.ni);
tempFlags.put("NGN", R.drawable.ng);
tempFlags.put("OMR", R.drawable.om);
tempFlags.put("PKR", R.drawable.pk);
tempFlags.put("PAB", R.drawable.pa);
tempFlags.put("PGK", R.drawable.pg);
tempFlags.put("PYG", R.drawable.py);
tempFlags.put("PEN", R.drawable.pe);
tempFlags.put("PHP", R.drawable.ph);
tempFlags.put("PLN", R.drawable.pl);
tempFlags.put("QAR", R.drawable.qa);
tempFlags.put("RON", R.drawable.ro);
tempFlags.put("RUB", R.drawable.ru);
tempFlags.put("RWF", R.drawable.rw);
tempFlags.put("STD", R.drawable.st);
tempFlags.put("SAR", R.drawable.sa);
tempFlags.put("SCR", R.drawable.sc);
tempFlags.put("SLL", R.drawable.sl);
tempFlags.put("SGD", R.drawable.sg);
tempFlags.put("SKK", R.drawable.sk);
tempFlags.put("SBD", R.drawable.sb);
tempFlags.put("SOS", R.drawable.so);
tempFlags.put("ZAR", R.drawable.za);
tempFlags.put("LKR", R.drawable.lk);
tempFlags.put("SDG", R.drawable.sd);
tempFlags.put("SRD", R.drawable.sr);
tempFlags.put("SZL", R.drawable.sz);
tempFlags.put("SEK", R.drawable.se);
tempFlags.put("SYP", R.drawable.sy);
tempFlags.put("TWD", R.drawable.tw);
tempFlags.put("TJS", R.drawable.tj);
tempFlags.put("TZS", R.drawable.tz);
tempFlags.put("THB", R.drawable.th);
tempFlags.put("TOP", R.drawable.to);
tempFlags.put("TTD", R.drawable.tt);
tempFlags.put("TND", R.drawable.tn);
tempFlags.put("TRY", R.drawable.tr);
tempFlags.put("TMT", R.drawable.tm);
tempFlags.put("UGX", R.drawable.ug);
tempFlags.put("UAH", R.drawable.ua);
tempFlags.put("AED", R.drawable.ae);
tempFlags.put("UYU", R.drawable.uy);
tempFlags.put("UZS", R.drawable.uz);
tempFlags.put("VUV", R.drawable.vu);
tempFlags.put("VEF", R.drawable.ve);
tempFlags.put("VND", R.drawable.vn);
tempFlags.put("YER", R.drawable.ye);
tempFlags.put("ZMK", R.drawable.zm);
tempFlags.put("ZWD", R.drawable.zw);
tempFlags.put("AOA", R.drawable.ao);
tempFlags.put("AQD", R.drawable.aq);
tempFlags.put("BAM", R.drawable.ba);
tempFlags.put("CDF", R.drawable.cd);
tempFlags.put("GHS", R.drawable.gh);
tempFlags.put("GGP", R.drawable.gg);
tempFlags.put("GBP", R.drawable.im);
tempFlags.put("LAK", R.drawable.la);
tempFlags.put("MOP", R.drawable.mo);
tempFlags.put("EUR", R.drawable.me);
tempFlags.put("JOD", R.drawable.ps);
tempFlags.put("RSD", R.drawable.rs);
tempFlags.put("USD", R.drawable.us);








        _instance = new FlagMapper(tempFlags);
    }

    public FlagMapper() {
        this.flags = new HashMap<>();
    }

    public FlagMapper(Map<String, Integer> flags) {
        this.flags = flags;
    }

    public static FlagMapper of() {
        return _instance;
    }

    public int getFlagId(String code){
        Integer id = flags.get(code);
        if(id == null){
            return -1;
        }
        return id;
    }
}



//tempFlags.put("###",R.drawable.Unspecified);

