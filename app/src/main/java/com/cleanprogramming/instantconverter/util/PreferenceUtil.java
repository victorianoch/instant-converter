package com.cleanprogramming.instantconverter.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.Nullable;

import java.util.Collections;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * Created by jerryzhang on 2/4/15.
 */
public class PreferenceUtil {

    private static String SETTING_PREFERENCE = "setting_preference";
    private static String USER_PREFERENCE = "USER_PREFERENCE";
    private static String SYSTEM_PREFERENCE = "SYSTEM_PREFERENCE";
    private static String LANGUAGE = "language";
    private static String LAST_VERSION_JSON = "last_version_json";
    private static String EMAIL = "email";
    private static String ADVERTISING_ID = "ADVERTISING_ID";
    private static final String SIGNUP_COMPLETE = "SIGNUP_COMPLETE";
    private static final String LAST_APP_UPDATE_NOTI_TIME_TAG = "LAST_APP_UPDATE_NOTI_TIME_TAG";
    private static final String CART_BADGE_NUM = "CART_BADGE_NUM";


    private static final String _DEFAULT_LANG = "English";

    public final static String LANGUAGE_UNSET = "";
    public final static String LANGUAGE_EN = "en_us";

    public final static String LANGUAGE_HANS = "zh";
    public final static String LANGUAGE_HANS1 = "zh_cn";
    public final static String LANGUAGE_HANS2 = "zh_CN";
    public final static String LANGUAGE_HANS3 = "rCN";

    public final static String LANGUAGE_HANT = "zh_hk";
    public final static String LANGUAGE_HANT1 = "zh_HK";
    public final static String LANGUAGE_HANT2 = "zh_TW";
    public final static String LANGUAGE_HANT3 = "zh-rHK";
    public final static String LANGUAGE_HANT4 = "zh-rTW";


    public final static String LANGUAGE_UNIFIED_ENG = "eng";
    public final static String LANGUAGE_UNIFIED_CHT = "cht";
    public final static String LANGUAGE_UNIFIED_CHS = "chs";


    public final static String UPDATE_TIMESTAMP = "UPDATE_TIMESTAMP";
    public final static String USER_TOKEN = "USER_TOKEN";

    public final static String USER_FACEBOOK_ID = "USER_FACEBOOK_ID";

    public final static String USER_FORGET_PW_EMAIL = "USER_FORGET_PW_EMAIL";

    public final static String USER_LEFT_OR_RIGHT = "USER_LEFT_OR_RIGHT";

    //screen size
    public final static String SCREEN_WIDTH = "SCREEN_WIDTH";
    public final static String SCREEN_HEIGHT = "SCREEN_HEIGHT";
    public final static String SCREEN_DIAGONAL = "SCREEN_DIAGONAL";



    //language mapping in order to convert to supported languages
    private static final Map<String, String> _languagesMap = sInitializeLanguageMap();


    /**************************************************************************
     **      LANGUAGE
     ***************************************************************************/
    /**
     * This initialize the values of {@link this.languagesMap}
     * @return
     */
    private static Map<String, String> sInitializeLanguageMap() {
        Map<String, String> result = new HashMap<String, String>();

        //english
        result.put(_DEFAULT_LANG, LANGUAGE_UNIFIED_ENG);
        result.put(LANGUAGE_EN, LANGUAGE_UNIFIED_ENG);

        //traditional chinese
        result.put(LANGUAGE_HANT, LANGUAGE_UNIFIED_CHT);
        result.put(LANGUAGE_HANT1, LANGUAGE_UNIFIED_CHT);
        result.put(LANGUAGE_HANT2, LANGUAGE_UNIFIED_CHT);
        result.put(LANGUAGE_HANT3, LANGUAGE_UNIFIED_CHT);
        result.put(LANGUAGE_HANT4, LANGUAGE_UNIFIED_CHT);

        //simplified chinese
        result.put(LANGUAGE_HANS, LANGUAGE_UNIFIED_CHS);
        result.put(LANGUAGE_HANS1, LANGUAGE_UNIFIED_CHS);
        result.put(LANGUAGE_HANS2, LANGUAGE_UNIFIED_CHS);
        result.put(LANGUAGE_HANS3, LANGUAGE_UNIFIED_CHS);

        return Collections.unmodifiableMap(result);
    }
    /**
     * currently only support three languages ENGLISH(default) / TRADITIONAL CHINESE / SIMPLIFIED CHINESE.
     * @return {@link this.DEFAULT_LANG} or specified system language code if available.
     */
    public static String getDisplayLanguage(@Nullable String lan) {
        String displayLanguage = "";
        if(lan == null){
            //get system language
            displayLanguage = _languagesMap.get(Locale.getDefault().toString());
        }else {
            //get custom language
            displayLanguage = _languagesMap.get(lan);
        }
        if(displayLanguage == null){
            displayLanguage = _languagesMap.get(_DEFAULT_LANG);
        }
        return displayLanguage;
    }

    public static String getLanguageFromPreference(Context context) {
        SharedPreferences preferences = getSharedPreference(context);
        String language = preferences.getString(LANGUAGE, LANGUAGE_UNSET);
        return language;
    }

    public static boolean isEng(Context context) {
        return PreferenceUtil.LANGUAGE_UNIFIED_ENG.equals( PreferenceUtil.getLanguageFromPreference( context ) );
    }

    public static void setLanguage(Context context, @Nullable String lanaugage) {
        SharedPreferences.Editor editor = getSharedPreferenceEditor(context);
        editor.putString(LANGUAGE, lanaugage);
        editor.commit();
    }


    /**************************************************************************
     * *     JSON
     ***************************************************************************/
    public static String getLastVersionJson(Context context) {
        SharedPreferences preferences = getSharedPreference(context);
        return preferences.getString(LAST_VERSION_JSON, "");
    }

    public static void setLastVersionJson(Context context, String json) {
        SharedPreferences.Editor editor = getSharedPreferenceEditor(context);
        editor.putString(LAST_VERSION_JSON, json);
        editor.commit();
    }


    /**************************************************************************
     * *    EMAIL
     ***************************************************************************/
    public static void setEmail(Context context, String email) {
        SharedPreferences.Editor editor = getSharedPreferenceEditor(context);
        editor.putString(EMAIL, email);
        editor.commit();
    }

    public static String getEmail(Context context) {
        SharedPreferences preferences = getSharedPreference(context);
        return preferences.getString(EMAIL, "");
    }


    /**************************************************************************
     * *      UPDATE TIMESTAMP
    ***************************************************************************/
    public static void setUpdateTimeStamp(Context context, Long timestamp) {
        SharedPreferences.Editor editor = getSharedPreferenceEditor(context);
        editor.putLong(UPDATE_TIMESTAMP, timestamp);
        editor.commit();
    }

    public static Long getUpdateTimeStamp(Context context) {
        SharedPreferences preferences = getSharedPreference(context);
        return preferences.getLong(UPDATE_TIMESTAMP, -1);
    }

    public static void deleteUpdateTimeStamp(Context context) {
        SharedPreferences.Editor editor = getSharedPreferenceEditor(context);
        editor.remove(UPDATE_TIMESTAMP);
        editor.commit();
    }




    /**************************************************************************
     * *      USER FACEBOOK ID
    ***************************************************************************/
    public static void setUserFacebookId(Context context, String userFacebookId) {
        SharedPreferences.Editor editor = getUserSharedPreferenceEditor(context);
        editor.putString(USER_FACEBOOK_ID, userFacebookId);
        editor.commit();
    }

    public static String getUserFacebookId(Context context) {
        SharedPreferences preferences = getUserSharedPreference(context);
        return preferences.getString(USER_FACEBOOK_ID, null);
    }

    public static void deleteUserFacebookId(Context context) {
        SharedPreferences.Editor editor = getUserSharedPreferenceEditor(context);
        editor.remove(USER_FACEBOOK_ID);
        editor.commit();
    }


    /**************************************************************************
     * *      USER FACEBOOK ID
    ***************************************************************************/
    public static void setUserForgetPwEmail(Context context, String email) {
        SharedPreferences.Editor editor = getUserSharedPreferenceEditor(context);
        editor.putString(USER_FORGET_PW_EMAIL, email);
        editor.commit();
    }

    public static String getUserForgetPwEmail(Context context) {
        SharedPreferences preferences = getUserSharedPreference(context);
        return preferences.getString(USER_FORGET_PW_EMAIL, null);
    }

    public static void deleteUserForgetPwEmail(Context context) {
        SharedPreferences.Editor editor = getUserSharedPreferenceEditor(context);
        editor.remove(USER_FORGET_PW_EMAIL);
        editor.commit();
    }


    /**************************************************************************
     * *      REMOTE CONFIG
    ***************************************************************************/
    public static void setRemoteConfig(Context context) {
        SharedPreferences.Editor editor = getSystemPreferenceEditor(context);
//        editor.putBoolean(PROMOTION_CODE, RemoteConfigHelper.REMOTE_CONFIG_STORAGE.promotion_code);
//        editor.putBoolean(COIN_TO_BE, RemoteConfigHelper.REMOTE_CONFIG_STORAGE.coin_to_be);
        editor.commit();
    }

    public static void getRemoteConfig(Context context) {
        SharedPreferences preferences = getSystemPreference(context);
//        RemoteConfigHelper.REMOTE_CONFIG_STORAGE.promotion_code = preferences.getBoolean(PROMOTION_CODE, );
    }

    public static void deleteRemoteConfig(Context context) {
        SharedPreferences.Editor editor = getSystemPreferenceEditor(context);
//        editor.remove(PROMOTION_CODE);
//        editor.remove(COIN_TO_BE);
        editor.commit();
    }

    /**************************************************************************
     * *      SCREEN SIZE
    ***************************************************************************/
    public static void setScreenWidth(Context context, int screenWidth) {
        SharedPreferences.Editor editor = getSystemPreferenceEditor(context);
        editor.putInt(SCREEN_WIDTH, screenWidth);
        editor.commit();
    }

    public static void setScreenHeight(Context context, int screenHeight) {
        SharedPreferences.Editor editor = getSystemPreferenceEditor(context);
        editor.putInt(SCREEN_HEIGHT, screenHeight);
        editor.commit();
    }

    public static void setScreenDiagonal(Context context, int screenDiagonal) {
        SharedPreferences.Editor editor = getSystemPreferenceEditor(context);
        editor.putInt(SCREEN_DIAGONAL, screenDiagonal);
        editor.commit();
    }

    public static int getScreenWidth(Context context) {
        SharedPreferences preferences = getSystemPreference(context);
        return preferences.getInt(SCREEN_WIDTH, -1);
    }

    public static int getScreenHeight(Context context) {
        SharedPreferences preferences = getSystemPreference(context);
        return preferences.getInt(SCREEN_HEIGHT, -1);
    }

    public static int getScreenDiagonal(Context context) {
        SharedPreferences preferences = getSystemPreference(context);
        return preferences.getInt(SCREEN_DIAGONAL, -1);
    }

    public static void deleteScreenWidth(Context context) {
        SharedPreferences.Editor editor = getSystemPreferenceEditor(context);
        editor.remove(SCREEN_WIDTH);
        editor.commit();
    }
    public static void deleteScreenHeight(Context context) {
        SharedPreferences.Editor editor = getSystemPreferenceEditor(context);
        editor.remove(SCREEN_HEIGHT);
        editor.commit();
    }
    public static void deleteScreenDiagonal(Context context) {
        SharedPreferences.Editor editor = getSystemPreferenceEditor(context);
        editor.remove(SCREEN_DIAGONAL);
        editor.commit();
    }



    /**************************************************************************
     **      SignUp flag
     ***************************************************************************/
    public static void setSignUpCompletedFlag(Context context, boolean flag){
        SharedPreferences.Editor editor = getSharedPreferenceEditor(context);
        editor.putBoolean(SIGNUP_COMPLETE, flag);
        editor.commit();
    }

    public static boolean getSignUpCompletedFlag(Context context){
        SharedPreferences preferences = getSharedPreference(context);
        return preferences.getBoolean(SIGNUP_COMPLETE, false);
    }

    public static void deleteSignUpCompletedFlag(Context context){
        SharedPreferences.Editor editor = getSharedPreferenceEditor(context);
        editor.remove(SIGNUP_COMPLETE);
        editor.commit();
    }


    /**************************************************************************
     **      Advertising ID
     ***************************************************************************/
    public static void setAdvertisingId(Context context, String advertId){
        SharedPreferences.Editor editor = getSharedPreferenceEditor(context);
        editor.putString(ADVERTISING_ID, advertId);
        editor.commit();
    }

    public static String getAdvertisingId(Context context){
        SharedPreferences preferences = getSharedPreference(context);
        return preferences.getString(ADVERTISING_ID, null);
    }



    /**************************************************************************
     **      Last time app update notification time tag
     ***************************************************************************/
    public static void setAppUpdateNotiTimeTag(Context context, long timeInSec){
        SharedPreferences.Editor editor = getSharedPreferenceEditor(context);
        editor.putLong(LAST_APP_UPDATE_NOTI_TIME_TAG, timeInSec);
        editor.commit();
    }

    public static long getAppUpdateNotiTimeTag(Context context){
        SharedPreferences preferences = getSharedPreference(context);
        return preferences.getLong(LAST_APP_UPDATE_NOTI_TIME_TAG, -1);
    }



    /**************************************************************************
     **      Cart badge number
     ***************************************************************************/
    public static void setCartBadge(Context context, int num){
        SharedPreferences.Editor editor = getSharedPreferenceEditor(context);
        editor.putInt(CART_BADGE_NUM, num);
        editor.commit();
    }

    public static int getCartBadge(Context context){
        SharedPreferences preferences = getSharedPreference(context);
        return preferences.getInt(CART_BADGE_NUM, -1);
    }

    public static void deleteCartBadge(Context context){
        SharedPreferences.Editor editor = getSharedPreferenceEditor(context);
        editor.remove(CART_BADGE_NUM);
        editor.commit();
    }



    /**************************************************************************
     * *      COMMON SHARED PREFERENCE METHOD
    ***************************************************************************/
    //setting
    private static SharedPreferences getSharedPreference(Context context) {
        return context.getSharedPreferences(SETTING_PREFERENCE, Context.MODE_PRIVATE);
    }

    private static SharedPreferences.Editor getSharedPreferenceEditor(Context context) {
        SharedPreferences sp = context.getSharedPreferences(SETTING_PREFERENCE, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        return context.getSharedPreferences(SETTING_PREFERENCE, Context.MODE_PRIVATE).edit();
    }

    //user
    private static SharedPreferences getUserSharedPreference(Context context) {
        return context.getSharedPreferences(USER_PREFERENCE, Context.MODE_PRIVATE);
    }

    private static SharedPreferences.Editor getUserSharedPreferenceEditor(Context context) {
        SharedPreferences sp = context.getSharedPreferences(USER_PREFERENCE, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        return context.getSharedPreferences(USER_PREFERENCE, Context.MODE_PRIVATE).edit();
    }

    //system ( administrator )
    private static SharedPreferences getSystemPreference(Context context) {
        return context.getSharedPreferences(SYSTEM_PREFERENCE, Context.MODE_PRIVATE);
    }

    private static SharedPreferences.Editor getSystemPreferenceEditor(Context context) {
        SharedPreferences sp = context.getSharedPreferences(SYSTEM_PREFERENCE, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        return context.getSharedPreferences(SYSTEM_PREFERENCE, Context.MODE_PRIVATE).edit();
    }
}
