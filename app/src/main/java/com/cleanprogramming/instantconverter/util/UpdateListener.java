package com.cleanprogramming.instantconverter.util;

/**
 * Created by Rainvic on 20/2/2017.
 */

public interface UpdateListener {
    void onSuccess();
    void onFailed();
}
