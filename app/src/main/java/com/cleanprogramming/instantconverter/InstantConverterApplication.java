package com.cleanprogramming.instantconverter;

import android.app.Application;

import com.cleanprogramming.instantconverter.constant.FlagMapper;

import io.realm.Realm;

/**
 * Created by Rainvic on 13/2/2017.
 */

public class InstantConverterApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();

        FlagMapper.init();
        Realm.init(this);

    }
}
