package com.cleanprogramming.instantconverter;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.transition.Explode;
import android.transition.Fade;
import android.transition.Slide;
import android.util.Pair;
import android.view.Gravity;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;

import java.util.ArrayList;
import java.util.List;

public class BaseActivity extends AppCompatActivity {

    private boolean isWaitingLoginActyResult = false;
    private boolean isActive = false;

    private static final int TRANSITION_ANIM_DURATION = 300;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

    }

    @Override
    protected void onResume() {
        super.onResume();
        this.isActive = true;
    }


    @Override
    protected void onPause() {
        super.onPause();
        this.isActive = false;
    }


    protected void setStatusbarColor(int color){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
        Window window = this.getWindow();

// clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

// add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

// finally change the color
            window.setStatusBarColor(color);
        }
    }

    /**************************************************************************
     * * Transition animation
     **************************************************************************/
    protected void transitionExplode(){

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

            // inside your activity (if you did not enable transitions in your theme)
            getWindow().requestFeature(Window.FEATURE_CONTENT_TRANSITIONS);

            Explode explode = new Explode();
            explode.setDuration(TRANSITION_ANIM_DURATION);


            // set an enter transition
            getWindow().setEnterTransition(explode);

            // set an exit transition
            getWindow().setExitTransition(explode);


            startPostPhonedEnterTransition();
        }
    }

    protected void transitionSlideup(){

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

            // inside your activity (if you did not enable transitions in your theme)
            getWindow().requestFeature(Window.FEATURE_CONTENT_TRANSITIONS);

            Slide up = new Slide(Gravity.BOTTOM);
            up.setDuration(TRANSITION_ANIM_DURATION);

            // set an enter transition
            getWindow().setEnterTransition(up);

            // set an exit transition
            getWindow().setExitTransition(up);

            startPostPhonedEnterTransition();

        }
    }

    protected void transitionSlideInFadeOut(){

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

            // inside your activity (if you did not enable transitions in your theme)
            getWindow().requestFeature(Window.FEATURE_CONTENT_TRANSITIONS);


            Slide up = new Slide(Gravity.BOTTOM);
            up.setDuration(TRANSITION_ANIM_DURATION);

            Fade out = new Fade(Fade.OUT);
            out.setDuration(TRANSITION_ANIM_DURATION);



            // set an enter transition
            getWindow().setEnterTransition(up);

            // set an exit transition
            getWindow().setExitTransition(out);

            startPostPhonedEnterTransition();

        }
    }

    protected void transitionFade(){

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

            // inside your activity (if you did not enable transitions in your theme)
            getWindow().requestFeature(Window.FEATURE_CONTENT_TRANSITIONS);

            Fade in = new Fade(Fade.IN);
            in.setDuration(TRANSITION_ANIM_DURATION);
            Fade out = new Fade(Fade.OUT);
            out.setDuration(TRANSITION_ANIM_DURATION);



            // set an enter transition
            getWindow().setEnterTransition(in);

            // set an exit transition
            getWindow().setExitTransition(out);

            startPostPhonedEnterTransition();

        }
    }

    protected void transitionEnterUpExitUp(){

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

            // inside your activity (if you did not enable transitions in your theme)
            getWindow().requestFeature(Window.FEATURE_CONTENT_TRANSITIONS);

            Slide up = new Slide(Gravity.BOTTOM);
            up.setDuration(TRANSITION_ANIM_DURATION);

            Slide down = new Slide(Gravity.TOP);
            down.setDuration(TRANSITION_ANIM_DURATION);

            // set an enter transition
            getWindow().setEnterTransition(up);

            // set an exit transition
            getWindow().setExitTransition(down);


            startPostPhonedEnterTransition();
        }
    }

    private void startPostPhonedEnterTransition(){
        supportPostponeEnterTransition();
//        new Handler().postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                supportStartPostponedEnterTransition();
//            }
//        }, TRANSITION_ANIM_DURATION);
        scheduleStartPostponedTransition(findViewById(android.R.id.content));
    }

    /**
     * Schedules the shared element transition to be started immediately
     * after the shared element has been measured and laid out within the
     * activity's view hierarchy. Some common places where it might make
     * sense to call this method are:
     *
     * (1) Inside a Fragment's onCreateView() method (if the shared element
     *     lives inside a Fragment hosted by the called Activity).
     *
     * (2) Inside a Picasso Callback object (if you need to wait for Picasso to
     *     asynchronously load/scale a bitmap before the transition can begin).
     *
     * (3) Inside a LoaderCallback's onLoadFinished() method (if the shared
     *     element depends on data queried by a Loader).
     */
    private void scheduleStartPostponedTransition(final View sharedElement) {
        sharedElement.getViewTreeObserver().addOnPreDrawListener(
                new ViewTreeObserver.OnPreDrawListener() {
                    @Override
                    public boolean onPreDraw() {
                        sharedElement.getViewTreeObserver().removeOnPreDrawListener(this);
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            startPostponedEnterTransition();
                        }
                        return true;
                    }
                });
    }




//    protected void startActivityForResultWithTransition(Activity activity, Intent intent, int requestCode){
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            startActivityForResult(intent, requestCode, ActivityOptions.makeSceneTransitionAnimation(activity).toBundle());
//        }else{
//            startActivityForResult(intent, requestCode);
//        }
//    }
    protected boolean isActivityActive(){
        return isActive;
    }

    protected void startActivityForResultWithTransition(Activity activity, Intent intent, int requestCode, Pair... sharedElements){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            View appbar = findViewById(R.id.toolbar);

            List<Pair> pairs = new ArrayList<>();
            for (Pair pair :
                    sharedElements) {
                pairs.add(pair);
            }

//            if(appbar != null) {
//                pairs.add(Pair.create(appbar, getString(R.string.transition_name_appbar)));
//            }

            Pair<View, String>[] pairs1 = new Pair[pairs.size()];

            startActivityForResult(intent, requestCode, ActivityOptions.makeSceneTransitionAnimation(activity, pairs.toArray(pairs1)).toBundle());
        }else{
            startActivityForResult(intent, requestCode);
        }
    }

}