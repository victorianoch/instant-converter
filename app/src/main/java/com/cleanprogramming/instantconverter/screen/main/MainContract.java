package com.cleanprogramming.instantconverter.screen.main;

import com.cleanprogramming.instantconverter.BasePresenter;
import com.cleanprogramming.instantconverter.BaseView;
import com.cleanprogramming.instantconverter.data.ModelCurrency;

import java.util.List;

/**
 * Created by Rainvic on 10/2/2017.
 */

public interface MainContract {
    interface View extends BaseView<Presenter>{
        void onBtnFabClicked();
        void showCurrencies(List<ModelCurrency> modelCurrencies);
    }
    interface Presenter extends BasePresenter{
        void btnFabClicked();
        void signInUser();
        void loadCurrencies();
        void updateCurrenciesFromServer();
    }
}
