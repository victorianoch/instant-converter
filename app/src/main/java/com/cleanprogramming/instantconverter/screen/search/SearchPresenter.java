package com.cleanprogramming.instantconverter.screen.search;

import android.util.Log;

import com.cleanprogramming.instantconverter.data.ModelCountry;
import com.cleanprogramming.instantconverter.data.RealMRepository;
import com.cleanprogramming.instantconverter.util.UpdateListener;

/**
 * Created by Rainvic on 14/2/2017.
 */

public class SearchPresenter implements SearchContract.Presenter {

    private static final String TAG = SearchPresenter.class.getSimpleName();

    private RealMRepository realMRepository;
    private SearchContract.View searchView;

    public SearchPresenter(RealMRepository realMRepository, SearchContract.View searchView) {
        this.realMRepository = realMRepository;
        this.searchView = searchView;
    }

    @Override
    public void start() {
        searchView.initSearchView();
    }

    @Override
    public void loadSearchResult(String keyword) {
        searchView.showSearchResult(realMRepository.queryStringForAllColumns(keyword));
    }

    @Override
    public void selectCurrency(ModelCountry modelCountry) {
        realMRepository.selectCurrency(modelCountry, new UpdateListener() {
            @Override
            public void onSuccess() {
                searchView.closeThisPage();
            }

            @Override
            public void onFailed() {
                searchView.closeThisPage();
            }
        });
    }
}
