package com.cleanprogramming.instantconverter.screen.search;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.cleanprogramming.instantconverter.R;
import com.cleanprogramming.instantconverter.adapter.HeaderRecyclerViewAdapterV2;
import com.cleanprogramming.instantconverter.data.ModelCountry;
import com.cleanprogramming.instantconverter.databinding.ItemSearchResultBinding;
import com.cleanprogramming.instantconverter.util.Util;

import java.util.List;



/**
 * Created by Rainvic on 15/2/2017.
 */

public class SearchResultAdapter extends HeaderRecyclerViewAdapterV2<SearchResultAdapter.BindingHolder> {

    private Context context;
    private List<ModelCountry> modelCountries;
    private ItemClickListener listener;

    public SearchResultAdapter(Context context, List<ModelCountry> modelCountries, ItemClickListener itemClickListener) {
        this.context = context;
        this.modelCountries = modelCountries;
        this.listener = itemClickListener;
    }

    /**************************************************************************
     * // Header
     **************************************************************************/

    @Override
    public boolean useHeader() {
        return false;
    }

    @Override
    public BindingHolder onCreateHeaderViewHolder(ViewGroup parent, int viewType) {
        return null;
    }

    @Override
    public void onBindHeaderView(BindingHolder holder, int position) {

    }


    /**************************************************************************
     * // Footer
     **************************************************************************/
    @Override
    public boolean useFooter() {
        return false;
    }

    @Override
    public BindingHolder onCreateFooterViewHolder(ViewGroup parent, int viewType) {
        return null;
    }

    @Override
    public void onBindFooterView(BindingHolder holder, int position) {

    }

    /**************************************************************************
     * // Basic
     **************************************************************************/
    @Override
    public BindingHolder onCreateBasicItemViewHolder(ViewGroup parent, int viewType) {
        ItemSearchResultBinding binding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.getContext()),
                R.layout.item_search_result,
                parent,
                false
        );
        return new BindingHolder(binding, new ItemClickListener() {
            @Override
            public void onItemClick(View itemView, int position) {
                listener.onItemClickReturnObj(itemView, getBasicItem(position));
            }

            @Override
            public void onItemClickReturnObj(View itemView, Object object) {
            }
        });
    }

    @Override
    public void onBindBasicItemView(BindingHolder holder, int position) {
        ModelCountry modelCountry = (ModelCountry) getBasicItem(position);
        ItemSearchResultBinding binding = holder.binding;
        if(modelCountry != null) {
            binding.tvResult.setText(modelCountry.getCountry() +"("+modelCountry.getCurrency()+")");
        }
    }

    @Override
    public int getBasicItemCount() {
        if(modelCountries == null) {
            return 0;
        }else{
            return modelCountries.size();
        }
    }

    @Override
    public int getBasicItemType(int position) {
        return 0;
    }

    @Override
    public Object getBasicItem(int position) {
        if(modelCountries ==null) {
            return null;
        }else{
            Integer index = Util.checkPositionIndex(position, modelCountries.size());
            if(index == null)
                return null;

            return modelCountries.get(index);
        }
    }

    static class BindingHolder extends RecyclerView.ViewHolder {
        private ItemSearchResultBinding binding;

        public BindingHolder(ItemSearchResultBinding binding, final ItemClickListener itemClickListener) {
            super(binding.getRoot());
            this.binding = binding;

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    itemClickListener.onItemClick(view, getAdapterPosition());
                }
            });
        }
    }

    public interface ItemClickListener{
        void onItemClick(View itemView, int position);
        void onItemClickReturnObj(View itemView, Object object);
    }
}
