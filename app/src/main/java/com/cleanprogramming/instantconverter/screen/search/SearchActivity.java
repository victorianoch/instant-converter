package com.cleanprogramming.instantconverter.screen.search;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;
import android.widget.SearchView;
import android.widget.SimpleAdapter;

import com.cleanprogramming.instantconverter.BaseActivity;
import com.cleanprogramming.instantconverter.R;
import com.cleanprogramming.instantconverter.data.ModelCountry;
import com.cleanprogramming.instantconverter.data.RealMRepository;
import com.cleanprogramming.instantconverter.databinding.ActivitySearchBinding;
import com.cleanprogramming.instantconverter.viewUtil.MyLinearLayoutManager;

import java.util.List;

public class SearchActivity extends BaseActivity implements SearchContract.View{

    private static final String TAG = SearchActivity.class.getSimpleName();

    public static final int REQUEST_CODE_SEARCH_ACTIVITY = 1001;

    private ActivitySearchBinding binding;

    private SearchContract.Presenter searchPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_search);

        //create the presenter
        searchPresenter = new SearchPresenter(RealMRepository.getINSTANCE(this), this);

        //bind up presenter
        binding.setPresenter(searchPresenter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        searchPresenter.start();
    }

    @Override
    public void showSearchResult(List<ModelCountry> modelCountryList) {
        binding.rvSearchResult.setHasFixedSize(true);
        binding.rvSearchResult.setAdapter(new SearchResultAdapter(this, modelCountryList, new SearchResultAdapter.ItemClickListener() {
            @Override
            public void onItemClick(View itemView, int position) {
                //not use
            }

            @Override
            public void onItemClickReturnObj(View itemView, Object object) {
                // TODO: 15/2/2017
                searchPresenter.selectCurrency((ModelCountry)object);
            }
        }));

        binding.rvSearchResult.setLayoutManager(new MyLinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
    }

    @Override
    public void initSearchView() {
        binding.svCurrency.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                searchPresenter.loadSearchResult(s);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                searchPresenter.loadSearchResult(s);
                return true;
            }
        });
    }

    @Override
    public void closeThisPage() {
        finish();
    }

    @Override
    public void setPresenter(SearchContract.Presenter presenter) {

    }

    @Override
    public void setLoadingIndicator(boolean active) {

    }

    @Override
    public boolean isActive() {
        return false;
    }
}
