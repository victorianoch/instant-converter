package com.cleanprogramming.instantconverter.screen.main;

import android.util.Log;

import com.cleanprogramming.instantconverter.constant.FlagMapper;
import com.cleanprogramming.instantconverter.data.FBRepository;
import com.cleanprogramming.instantconverter.data.ModelCurrency;
import com.cleanprogramming.instantconverter.data.RealMRepository;
import com.cleanprogramming.instantconverter.util.UpdateListener;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.RealmResults;

/**
 * Created by Rainvic on 10/2/2017.
 */

public class MainPresenter implements MainContract.Presenter {

    private static final String TAG = MainPresenter.class.getSimpleName();
    private MainContract.View mainView;
    private RealMRepository realMRepository;
    private FBRepository fbRepository;
    private static boolean isFristTimeLoading = true;

    public MainPresenter(MainContract.View mainView, FBRepository fbRepository, RealMRepository realMRepository) {
        this.mainView = mainView;
        this.fbRepository = fbRepository;
        this.realMRepository = realMRepository;
        this.mainView.setPresenter(this);
    }

    @Override
    public void start() {
        if(isFristTimeLoading) {
            signInUser();
            isFristTimeLoading = false;
        }else{
            loadCurrencies();
        }
    }

    @Override
    public void btnFabClicked() {
        mainView.onBtnFabClicked();
    }

    @Override
    public void signInUser() {
        fbRepository.signInUser(new UpdateListener() {
            @Override
            public void onSuccess() {
                updateCurrenciesFromServer();
            }

            @Override
            public void onFailed() {
                Log.e(TAG, "onUpdateFailed: ", null);
            }
        });
    }

    @Override
    public void loadCurrencies() {
        List<ModelCurrency> modelCurrencyList = realMRepository.getSelectedCurrencies();
        mainView.showCurrencies(modelCurrencyList);
    }

    @Override
    public void updateCurrenciesFromServer() {
        fbRepository.checkUpdateAvailableNUpdate(new UpdateListener() {
            @Override
            public void onSuccess() {
                loadCurrencies();
            }

            @Override
            public void onFailed() {
                loadCurrencies();
            }
        });
    }
}
