package com.cleanprogramming.instantconverter.screen.main;

import android.content.Context;
import android.databinding.BaseObservable;
import android.renderscript.BaseObj;

/**
 * Created by Rainvic on 10/2/2017.
 */

public class MainViewModel extends BaseObservable {
    private Context context;
    private MainContract.Presenter mainPresenter;

    public MainViewModel(Context context, MainContract.Presenter mainPresenter) {
        this.context = context;
        this.mainPresenter = mainPresenter;
    }
}
