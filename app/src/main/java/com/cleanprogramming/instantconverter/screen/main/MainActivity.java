package com.cleanprogramming.instantconverter.screen.main;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.cleanprogramming.instantconverter.BaseActivity;
import com.cleanprogramming.instantconverter.R;
import com.cleanprogramming.instantconverter.data.FBRepository;
import com.cleanprogramming.instantconverter.data.ModelCurrency;
import com.cleanprogramming.instantconverter.data.RealMRepository;
import com.cleanprogramming.instantconverter.databinding.ActivityMainBinding;
import com.cleanprogramming.instantconverter.screen.search.SearchActivity;
import com.cleanprogramming.instantconverter.viewUtil.MyLinearLayoutManager;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.math.BigDecimal;
import java.util.List;

import static com.cleanprogramming.instantconverter.screen.search.SearchActivity.REQUEST_CODE_SEARCH_ACTIVITY;

public class MainActivity extends BaseActivity implements MainContract.View{
    private static final String TAG = MainActivity.class.getSimpleName();

    private ActivityMainBinding binding;

    private MainContract.Presenter mainPresenter;

    private MainViewModel mainViewModel;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);

        //create the presenter
        mainPresenter = new MainPresenter(this, FBRepository.getInstance(this), RealMRepository.getINSTANCE(this));

        //creat View model
        mainViewModel = new MainViewModel(this, mainPresenter);

        //bind up viewmodel and presenter
        binding.setPresenter(mainPresenter);
        binding.setViewModel(mainViewModel);

//        setSupportActionBar(binding.toolbar);



    }

    @Override
    protected void onResume() {
        super.onResume();
        mainPresenter.start();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBtnFabClicked() {
//        FBRepository.getInstance(this).getCurrencies();
//        Snackbar.make(findViewById(android.R.id.content), "FBRepository.getInstance(this).getCurrencies()", Snackbar.LENGTH_LONG)
//                .setAction("Action", null).show();

        Intent intent = new Intent(this, SearchActivity.class);
        startActivityForResult(intent, REQUEST_CODE_SEARCH_ACTIVITY);
    }

    @Override
    public void showCurrencies(List<ModelCurrency> modelCurrencies) {

        //set selected tags recyclerview
        binding.rvInputs.setHasFixedSize(true);
        binding.rvInputs.setAdapter(new CurrencyAdapter(this, modelCurrencies, BigDecimal.ZERO));
        binding.rvInputs.setLayoutManager(new MyLinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
    }

    @Override
    public void setPresenter(MainContract.Presenter presenter) {

    }

    @Override
    public void setLoadingIndicator(boolean active) {

    }

    @Override
    public boolean isActive() {
        return false;
    }


    /**************************************************************************
     // private
     **************************************************************************/
}
