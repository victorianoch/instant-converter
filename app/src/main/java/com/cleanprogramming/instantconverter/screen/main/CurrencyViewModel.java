package com.cleanprogramming.instantconverter.screen.main;

import android.content.Context;
import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;

import com.cleanprogramming.instantconverter.BR;
import com.cleanprogramming.instantconverter.constant.FlagMapper;
import com.cleanprogramming.instantconverter.data.ModelCurrency;

import java.math.BigDecimal;

/**
 * Created by Rainvic on 13/2/2017.
 */

public class CurrencyViewModel extends BaseObservable {
    private Context context;
    private ModelCurrency modelCurrency;
    private BigDecimal currentValue;

    public CurrencyViewModel(Context context, ModelCurrency modelCurrency, BigDecimal currentValue) {
        this.context = context;
        this.modelCurrency = modelCurrency;
        this.currentValue = currentValue;
    }

    @Bindable
    public Drawable getFlag(){
        if(modelCurrency != null){
            int resId = FlagMapper.of().getFlagId(modelCurrency.getTarget().toUpperCase());
            if(resId > 0) {
                Drawable flag = ContextCompat.getDrawable(context, resId);
                return flag;
            }else{
                return null;
            }
        }
        return null;
    }

    @Bindable
    public String getValue(){
        if(currentValue != null){
            return currentValue.setScale(4, BigDecimal.ROUND_DOWN).toString();
        }
        return null;
    }

//    public void setValue(String value){
//        currentValue = new BigDecimal(value);
//    }

    @Bindable
    public String getCurrency(){
        if(modelCurrency != null){
            return modelCurrency.getTarget().toUpperCase();
        }
        return null;
    }

    public void setCurrentValue(BigDecimal newVal){
        currentValue = newVal;
        notifyPropertyChanged(BR.value);
    }


}
