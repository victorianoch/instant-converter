package com.cleanprogramming.instantconverter.screen.main;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.cleanprogramming.instantconverter.R;
import com.cleanprogramming.instantconverter.adapter.HeaderRecyclerViewAdapterV2;
import com.cleanprogramming.instantconverter.data.ModelCurrency;
import com.cleanprogramming.instantconverter.databinding.ItemSourceCurrencyBinding;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by Rainvic on 13/2/2017.
 */

public class CurrencyAdapter extends HeaderRecyclerViewAdapterV2<CurrencyAdapter.BindingHolder> {

    private Context context;
    private List<ModelCurrency> currencies;
    private BigDecimal curSourceVal;

    public CurrencyAdapter(Context context, List<ModelCurrency> currencies, BigDecimal curSourceVal) {
        this.context = context;
        this.currencies = currencies;
        this.curSourceVal = curSourceVal;
    }

    /**************************************************************************
     * // Header
     **************************************************************************/
    @Override
    public boolean useHeader() {
        return false;
    }

    @Override
    public BindingHolder onCreateHeaderViewHolder(ViewGroup parent, int viewType) {
        return null;
    }

    @Override
    public void onBindHeaderView(BindingHolder holder, int position) {

    }

    /**************************************************************************
     * // Footer
     **************************************************************************/
    @Override
    public boolean useFooter() {
        return false;
    }

    @Override
    public BindingHolder onCreateFooterViewHolder(ViewGroup parent, int viewType) {
        return null;
    }

    @Override
    public void onBindFooterView(BindingHolder holder, int position) {

    }

    /**************************************************************************
     * // Basic
     **************************************************************************/
    @Override
    public BindingHolder onCreateBasicItemViewHolder(ViewGroup parent, int viewType) {
        ItemSourceCurrencyBinding itemSourceCurrencyBinding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.getContext()),
                R.layout.item_source_currency,
                parent,
                false
        );
        return new BindingHolder(itemSourceCurrencyBinding, new OnValueChanged() {
            @Override
            public void onValueChanged(View itemView, int changedValue, int position) {
                if(position != RecyclerView.NO_POSITION) {
                    ModelCurrency modelCurrency = (ModelCurrency) getBasicItem(position);
                    if(curSourceVal.setScale(4, BigDecimal.ROUND_DOWN).compareTo(new BigDecimal((double) changedValue / modelCurrency.getRate()).setScale(4, BigDecimal.ROUND_DOWN))
                        != 0) {
                        curSourceVal = new BigDecimal((double) changedValue / modelCurrency.getRate());

                        for(int i=0; i<currencies.size(); i++){
                            if(i != position)
                                notifyItemChanged(i);
                        }
//                        notifyDataSetChanged();
                    }
                }
            }
        });
    }

    @Override
    public void onBindBasicItemView(BindingHolder holder, int position) {
        ModelCurrency modelCurrency = (ModelCurrency) getBasicItem(position);
        ItemSourceCurrencyBinding itemSourceCurrencyBinding = holder.itemSourceCurrencyBinding;
        itemSourceCurrencyBinding.setViewModel(new CurrencyViewModel(context, modelCurrency, curSourceVal.multiply(new BigDecimal(modelCurrency.getRate()))));
    }

    @Override
    public int getBasicItemCount() {
        if(currencies == null){
            return 0;
        }
        return currencies.size();
    }

    @Override
    public int getBasicItemType(int position) {
        return 0;
    }

    @Override
    public Object getBasicItem(int position) {
        if(currencies == null){
            return null;
        }
        return currencies.get(position);
    }

    static class BindingHolder extends RecyclerView.ViewHolder {
        private ItemSourceCurrencyBinding itemSourceCurrencyBinding;

        public BindingHolder(ItemSourceCurrencyBinding binding, final OnValueChanged onValueChanged) {
            super(binding.getRoot());
            this.itemSourceCurrencyBinding = binding;

            itemSourceCurrencyBinding.etValue.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    if(itemSourceCurrencyBinding.etValue.hasFocus()) {
                        int pos = getAdapterPosition();
                        int changedInt = -1;
                        try {
                            if(charSequence.toString().isEmpty()){
                                changedInt = 0;
                            }else {
                                changedInt = Integer.parseInt(charSequence.toString());
                            }
                        } catch (NumberFormatException e) {

                        } finally {
                            if (changedInt > -1)
                                onValueChanged.onValueChanged(itemView, changedInt, pos);
                        }
                    }
                }

                @Override
                public void afterTextChanged(Editable editable) {
                    String txt = editable.toString();

                    if(txt.contains("0.0000")){
                        txt = txt.replace("0.0000", "");
                        itemSourceCurrencyBinding.etValue.setText(txt);
                    }
                }
            });
        }
    }

    public interface OnValueChanged{
        public void onValueChanged(View itemView, int changedValue, int position);
    }
}
