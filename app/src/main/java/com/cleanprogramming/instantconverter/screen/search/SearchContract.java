package com.cleanprogramming.instantconverter.screen.search;

import com.cleanprogramming.instantconverter.BasePresenter;
import com.cleanprogramming.instantconverter.BaseView;
import com.cleanprogramming.instantconverter.data.ModelCountry;

import java.util.List;

/**
 * Created by Rainvic on 14/2/2017.
 */

public interface SearchContract {
    interface View extends BaseView<Presenter>{
        void showSearchResult(List<ModelCountry> modelCountryList);
        void initSearchView();
        void closeThisPage();
    }
    interface Presenter extends BasePresenter{
        void loadSearchResult(String keyword);
        void selectCurrency(ModelCountry modelCountry);

    }
}
